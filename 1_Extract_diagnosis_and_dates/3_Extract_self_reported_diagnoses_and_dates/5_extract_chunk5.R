library(dplyr)

# Read a chunk of biobank data 
chunk5<-read.csv("~/Project/ukb.csv",nrows=100000,skip=400001,check.names = FALSE,header=FALSE)

# Get headers 
headerchunk<-read.csv("~/Project/ukb.csv",nrows=2,skip=0,check.names = FALSE)
headers<-names(headerchunk)
colnames(chunk5) = headers 

# Select data fields for self-reported diagnoses
diagofinterest<-function(x) {
  x == 1262 | x == 1263
}
chunk5selfreport <- chunk5 %>% select(c("eid","34-0.0",
                                        starts_with("87"),
                                        starts_with("20002"))) %>%
  filter(if_any(starts_with("20002"),diagofinterest))
saveRDS(chunk5selfreport, file = "~/Scratch/selfreport5.rds")
