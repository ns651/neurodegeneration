---
title: "R Notebook"
output:
  html_document:
    df_print: paged
---

Load data and tidy up  
```{r}
library(lubridate)
library(dplyr)

# Load data extracted from chunks 
baseline1<-readRDS(file = "~/Scratch/baseline1.rds")
baseline2<-readRDS(file = "~/Scratch/baseline2.rds")
baseline3<-readRDS(file = "~/Scratch/baseline3.rds")
baseline4<-readRDS(file = "~/Scratch/baseline4.rds")
baseline5<-readRDS(file = "~/Scratch/baseline5.rds")
baseline6<-readRDS(file = "~/Scratch/baseline6.rds")

# Combine chunks 
baselinedata<-base::rbind(baseline1,baseline2,baseline3,baseline4,baseline5,baseline6)

# Rename columns
fields_coding<-read.csv("~/Project/fields_coding.csv")
colnames(baselinedata)<-fields_coding$meaning[match(names(baselinedata),fields_coding$code)]
```

Incorporate diagnoses and diagnosis dates
```{r}
# Add in formal and self-reported diagnoses 
diag<-read.csv("~/Project/DegenDiag.csv")
srdiag<-read.csv("~/Project/selfreportdata.csv")
baselinediag<-baselinedata %>%
  full_join(diag,by="eid") %>%
  full_join(srdiag,by="eid")

# Compare baseline assessment date with diagnosis dates
baselinediag <- baselinediag %>%
  mutate(date_of_assessment_0 = ymd(date_of_assessment_0)) %>%
  mutate(diagdate = ymd(diagdate)) %>%
  mutate(days_post_diag = as.numeric(date_of_assessment_0) - as.numeric(diagdate)) %>%
  mutate(baselineyear = year(date_of_assessment_0)) %>%
  mutate(sr_years_post_diag = baselineyear - sr_year) 

# Recode post-diagnosis time = NA as -999999
baselinediag$days_post_diag[is.na(baselinediag$days_post_diag)]<--999999
baselinediag$sr_years_post_diag[is.na(baselinediag$sr_years_post_diag)]<--999999

# Filter out those with diagnosis at baseline 
baselinediag<-baselinediag %>%
 filter(days_post_diag < 0) %>%
  filter(sr_years_post_diag < 0) 

# Define controls
baselinediag$diag[is.na(baselinediag$diag)]<-"control"

# Filter out excluded diagnoses
baselinediag<-baselinediag %>%
  filter(diag == "control" 
         | diag == "alz"
         | diag == "psp"
         | diag == "ftd"
         | diag == "dlb"
         | diag == "pd"
         | diag == "msa"
  )

# Filter out controls with a future self-reported diagnosis 
removecontrols<-baselinediag %>%
  filter(diag =="control")%>%
  filter(eid %in% srdiag$eid)
baselinediag<-baselinediag %>%
  filter(!eid %in% removecontrols$eid)
```

Export final table 
```{r}
# Drop extraneous columns, rename variables 
baselinediag <- baselinediag %>%
  select(!c(diagdate,sr_diag,sr_year,days_post_diag,baselineyear,sr_years_post_diag)) %>%
  rename("age"="age_0") %>%
  rename("Diagnosis" = "diag")%>%
  mutate(Diagnosis = case_when(
    Diagnosis == "control" ~ "Control",
    Diagnosis == "alz" ~ "AD",  
    Diagnosis == "ftd" ~ "FTD",
    Diagnosis == "pd" ~ "PD",
    Diagnosis == "psp" ~ "PSP",
    Diagnosis == "msa" ~ "MSA",
    Diagnosis == "dlb" ~ "DLB"
  ))

# Export csv file 
write.csv(baselinediag,"~/Project/baselinedata.csv", row.names = FALSE)

# Save progress
save.image(file="~/Project/BaselineDataExtraction.RData")
```




