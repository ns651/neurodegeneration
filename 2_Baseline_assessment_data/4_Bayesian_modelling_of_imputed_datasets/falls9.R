library(mice)
library(brms)
library(dplyr)

load("~/ImputedData.RData")
falls_priors<- c(prior(cauchy(0,5), class = b))
falls9<-brm(Falls_in_last_year_0 ~ age,
            data=complete4,
            family = cumulative,  
            iter=3500,warmup=1000,chains=4,cores=4,
            prior= falls_priors,
            save_pars = save_pars(all=TRUE),
            file="falls9")