library(mice)
library(brms)
library(dplyr)

load("~/ImputedData.RData")
health_priors<-c(prior(cauchy(0,5),class=b))
health2<-brm(overall_health_rating_0 ~ age + Diagnosis,
             data=complete2,
             family = cumulative,  
             iter=3500,warmup=1000,chains=4,cores=4,
             prior=health_priors,
             save_pars = save_pars(all=TRUE),
             file="health2")