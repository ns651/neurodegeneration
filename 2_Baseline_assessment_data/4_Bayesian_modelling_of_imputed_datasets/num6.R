library(mice)
library(brms)
library(dplyr)

load("~/ImputedData.RData")
numeric_priors<-c(prior(cauchy(0,10),class="b"))
num6<-brm(numeric_memory_max_digits_remembered_0 ~ age,
          data=complete1,
          family = gaussian(),  
          iter=11000,warmup=1000,chains=5,cores=5,
          prior=numeric_priors,
          save_pars = save_pars(all=TRUE),
          file="num6")