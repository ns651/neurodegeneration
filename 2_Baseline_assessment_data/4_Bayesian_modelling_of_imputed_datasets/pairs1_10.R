library(mice)
library(brms)
library(dplyr)

load("~/ImputedData.RData")
pairs1_priors<-c(prior(cauchy(0,5),class="b"))
pairs110<-brm(pairs_matching_0_1 ~ age,
              data= complete5,
              family = negbinomial(),  
              iter=3500,warmup=1000,chains=4,cores=4,
              prior=pairs1_priors,
              save_pars = save_pars(all=TRUE),
              file="pairs1_10")