library(mice)
library(brms)
library(dplyr)

load("~/ImputedData.RData")
pairs2_priors<-c(prior(cauchy(0,5),class="b"))
pairs26<-brm(pairs_matching_0_2 ~ age,
             data= complete1,
             family = negbinomial(),  
             iter=2000,warmup=1000,chains=10,cores=10,
             prior=pairs2_priors,
             save_pars = save_pars(all=TRUE),
             file="pairs2_6")