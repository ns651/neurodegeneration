library(mice)
library(brms)
library(dplyr)

load("~/ImputedData.RData")

prospective_priors<- c(prior(cauchy(0,5), class = b))
pros6<-brm(prospective_memory_0 ~ age,
           data= complete1,
           family = cumulative,  
           iter=3500,warmup=1000,chains=20,cores=20,
           prior=prospective_priors,
           save_pars = save_pars(all=TRUE),
           file="pros6")
