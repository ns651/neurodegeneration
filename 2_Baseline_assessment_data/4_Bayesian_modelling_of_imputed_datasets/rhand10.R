library(mice)
library(brms)
library(dplyr)

load("ImputedData.RData")
rhand_priors<-c(prior(cauchy(0,90),class="b"))

#Set Initialisation Values
sample_priors <- function() {
  sample <- 
    list(
      "Intercept" = 0, 
      "sigma" = 0, 
      "alpha" = 0
    )
  
  while (!sample$Intercept > 0)
    sample$Intercept  <- rnorm(1, 40, 10) 
  
  while (!sample$sigma > 0)
    sample$sigma <- rnorm(1, 11, 1) 
  
  sample$alpha <- rnorm(1, 2.0,0.5)   
  
  return (sample)  
}

N_chains <- 10
init_values <- vector("list", N_chains)
for (n in 1:N_chains) init_values[[n]] <- sample_priors()


rhand10<-brm(hand_grip_right_0 ~ age + handedness,
            data=complete5,
            family = "skew_normal",  
            iter=2000,warmup=1000,chains=10,cores=10,
            prior=rhand_priors,
            save_pars = save_pars(all=TRUE),
            inits=init_values,
            file="rhand10")
