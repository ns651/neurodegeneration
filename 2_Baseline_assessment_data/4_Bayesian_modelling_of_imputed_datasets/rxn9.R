library(mice)
library(brms)
library(dplyr)

load("~/ImputedData.RData")
reaction_priors<-c(prior(cauchy(0,5),class="b"))
rxn9<-brm(reaction_time_mean_time_to_correctly_identify_matches_0 ~ age,
           data=complete4,
           family = "shifted_lognormal",  
           iter=2000,warmup=1000,chains=10,cores=10,
           prior=reaction_priors,
           save_pars = save_pars(all=TRUE),
           file="rxn9")