library(mice)
library(brms)
library(dplyr)

load("~/ImputedData.RData")
weight_priors<- c(prior(cauchy(0,5), class = b, dpar = mu2),
                  prior(cauchy(0,5), class = b, dpar = mu3))
weight7<-brm(weight_change_compared_with_1_year_ago_0 ~ age,
             data=complete2,
             family = categorical,  
             iter=2000,warmup=1000,chains=10,cores=10,
             prior=weight_priors,
             save_pars = save_pars(all=TRUE),
             file="weight7")