---
title: "R Notebook"
output:
  html_document:
    df_print: paged
---

```{r}
extraction<-function(x) {
  extracted<-x %>%
    gather_draws(b_DiagnosisAD,b_DiagnosisFTD,b_DiagnosisPD,b_DiagnosisPSP,b_DiagnosisDLB,b_DiagnosisMSA) 
  extracted$.variable<-factor(extracted$.variable, levels=c("b_DiagnosisAD","b_DiagnosisFTD","b_DiagnosisPD","b_DiagnosisPSP","b_DiagnosisDLB","b_DiagnosisMSA","b_age"))
  return(extracted)}

plotting<-function(x,ropemin,ropemax,middle){
  x %>% ggplot(aes(y = .variable, x = .value, xmin = .lower, xmax = .upper)) +
    geom_rect(aes(xmin=ropemin, xmax=ropemax, ymin=-Inf, ymax=Inf),alpha=0.07,fill="#E69F00")+
    geom_vline(xintercept = ropemin, linetype="dashed",color = "dodgerblue2",size=0.7)+
    geom_vline(xintercept = ropemax, linetype="dashed",color = "dodgerblue2",size=0.7)+
    geom_vline(xintercept = middle, linetype="solid",color = "#000000",size=0.5)+
    geom_pointinterval(point_size=3,interval_size_range=c(0.75,1.75)) +
    scale_y_discrete(labels=c("MSA","DLB","PSP","PD","FTD","AD"),limits=rev) +
    labs(y="Future Diagnosis") +
    theme(
      plot.title = element_text(hjust=0.5 ,size=11, face="bold"),
      plot.subtitle = element_text(size=7,hjust=0),
      axis.text.x= element_text(size=9),
      axis.text.y= element_text(size=9),
      axis.title.x = element_text(size=10,margin = margin(t = 5)),
      axis.title.y=element_text(size=10,margin = margin(r = 5)),
      panel.background = element_rect(fill="grey95", colour=NA),
      panel.border = element_rect(colour = "black", fill=NA, size=0.5),
      panel.grid.major = element_line(color="white"),
      plot.margin = margin(t=0,b=20,l=10,r=10))
}

printing<-function(x){
  saveRDS(x,file=paste0("~/",currentname,"plot.rds",sep=""))
}

save(extraction,plotting,printing,file="~/Project/commonfx.RData")
```
