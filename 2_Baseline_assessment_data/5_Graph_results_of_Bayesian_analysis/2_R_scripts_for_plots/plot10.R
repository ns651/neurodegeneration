library(brms)
library(ggplot2)
library(tidybayes)
library(forcats)
library(dplyr)

currentname<-"pros"
load("~/commonfx.RData")

for (i in 1:1){
  assign(paste0("model",i),
         readRDS(paste0("~/",currentname,i,".rds",sep="")))
}
full<-combine_models(model1)
extract<-extraction(full)%>%
  mutate(.value = exp(-.value)) %>% 
  mean_qi(.width=c(0.95,0.5))
plot<-plotting(extract,
               ropemin=exp(-0.18),
               ropemax=exp(0.18),
               middle=1)+
  scale_x_log10()+
  labs(title="Prospective Memory",
       x="Multiplicative Effect on\nOdds of Better Recall",
       subtitle = "AD (n=746), FTD (n=55), PD (n=720), PSP (n=50),\nDLB (n=9), MSA (n=30), Control (n=168878)") 
printing(plot)