library(brms)
library(ggplot2)
library(tidybayes)
library(forcats)
library(dplyr)

currentname<-"pairs1_"
load("~/commonfx.RData")

for (i in 1:5){
  assign(paste0("model",i),
         readRDS(paste0("~/",currentname,i,".rds",sep="")))
}
full<-combine_models(model1,model2,model3,model4,model5,check_data = FALSE)
extract<-extraction(full) %>%
  mutate(.value = exp(.value)) %>% 
  mean_qi(.value,.width=c(0.95,0.5))
plot<-plotting(extract,ropemin=1-0.1,
               ropemax=1+0.1,
               middle=1)+
  labs(title="Pairs Matching - Round 1",
       x="Multiplicative Effect on\nNo. of Incorrect Matches",
       subtitle = "AD (n=2727), FTD (n=210), PD (n=2336), PSP (n=129),\nDLB (n=40), MSA (n=73), Control (n=489238)") 
printing(plot)
