library(brms)
library(ggplot2)
library(tidybayes)
library(forcats)
library(dplyr)

currentname<-"rxn"
load("~/commonfx.RData")

for (i in 1:5){
  assign(paste0("model",i),
         readRDS(paste0("~/",currentname,i,".rds",sep="")))
}
full<-combine_models(model1,model2,model3,model4,model5,check_data = FALSE)
extract<-extraction(full) %>%
  mean_qi(.value,.width=c(0.95,0.5)) 
sd<-full %>% gather_draws(sigma) %>% summarise(sd = mean(.value))
plot<-plotting(extract,ropemin=0-0.1*sd$sd,
               ropemax=0+0.1*sd$sd,
               middle=0)+
  labs(title="Reaction Time (log-transformed)",
       x="Numerical Difference vs Control Mean\n(on log-transformed scale)",
       subtitle = "AD (n=2690), FTD (n=201), PD (n=2317), PSP (n=128),\nDLB (n=39), MSA (n=72), Control (n=488183)") 
printing(plot)