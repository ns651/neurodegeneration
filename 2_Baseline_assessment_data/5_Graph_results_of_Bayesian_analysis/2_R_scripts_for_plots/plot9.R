library(brms)
library(ggplot2)
library(tidybayes)
library(forcats)
library(dplyr)

currentname<-"num"
load("~/commonfx.RData")

for (i in 1:1){
  assign(paste0("model",i),
         readRDS(paste0("~/",currentname,i,".rds",sep="")))
}
full<-combine_models(model1)
extract<-extraction(full) %>% 
  mean_qi(.value,.width=c(0.95,0.5)) 
sd<-full %>% gather_draws(sigma) %>% summarise(sd = mean(.value))
plot<-plotting(extract,ropemin=0-0.1*sd$sd,
               ropemax=0+0.1*sd$sd,
               middle=0)+
  labs(title="Max No. of Digits Remembered",
       x="Numerical Difference vs Control Mean",
       subtitle = "AD (n=244), FTD (n=19), PD (n=214), PSP (n=15),\nDLB (n=4), MSA (n=8), Control (n=49564)") 
printing(plot)