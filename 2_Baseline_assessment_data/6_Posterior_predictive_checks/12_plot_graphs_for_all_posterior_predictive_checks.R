library(brms)
library(bayesplot)
library(ggplot2)
library(dplyr)

# Load pre-saved data 
load("~/ImputedData.RData")
for (i in 1:11){
  assign(paste0("yrep",i),
         readRDS(paste0("~/","~","yrep",i,sep="")))
}

# Set colour scheme and theme of plots 
color_scheme_set("brightblue")
ppcplot<-function(x){
  x + 
    theme_bw() +
    theme(plot.title = element_text(hjust=0.5 ,size=12, face="bold"),
          plot.subtitle = element_text(hjust=0.5,size=11),
          axis.text = element_text(size=10),
          legend.text.align = 0,
          legend.spacing.y = unit(8,"pt"))+
    facet_text(size = rel(0.9))  
}

# PPC1 - Falls in Last Year
falls_na<-which(is.na(BaselineData$Falls_in_last_year_0))
yrep1<-yrep1[,-falls_na]
y1<-BaselineData %>%
  select(c("Falls_in_last_year_0","Diagnosis")) %>%
  filter(!is.na(Falls_in_last_year_0))%>%
  mutate(Falls_in_last_year_0 = as.numeric(Falls_in_last_year_0)) 
ppc1<-ppc_bars_grouped(y1$Falls_in_last_year_0,
                       yrep1,
                       group=y1$Diagnosis,
                       freq=FALSE,
                       prob=0.95) + 
  labs(title="Falls in Last Year") +
  scale_x_continuous(breaks=c(1,2,3),
                     labels=c("0 falls","1 fall", ">1 fall"))
ppcplot1<-ppcplot(ppc1)
ggsave(file="~/ppcplot1.png",
       ppcplot1,width=200,height=130,units="mm")

# PPC2 - Fluid Intelligence Score 
yrep2<-round(yrep2)
y2<-BaselineData %>%
  select(c("fluid_intelligence_score_0","Diagnosis")) %>% 
  filter(!is.na(fluid_intelligence_score_0)) 
ppc2<-ppc_bars_grouped(y2$fluid_intelligence_score_0,
                       yrep2,
                       group=y2$Diagnosis,
                       freq=FALSE,
                       prob=0.95)+
  labs(title="Fluid Intelligence Score")
ppcplot2<-ppcplot(ppc2)
ggsave(file="~/ppcplot2.png",
       ppcplot2,width=200,height=130,units="mm")

# PPC3 - Overall Health Rating
health_na<-which(is.na(BaselineData$overall_health_rating_0))
yrep3<-yrep3[,-health_na]
y3<-BaselineData %>%
  select(c("overall_health_rating_0","Diagnosis")) %>%
  filter(!is.na(overall_health_rating_0)) %>% 
  mutate(overall_health_rating_0 = as.numeric(overall_health_rating_0)) 
ppc3<-ppc_bars_grouped(y3$overall_health_rating_0,
                       yrep3,
                       group=y3$Diagnosis,
                       freq=FALSE,
                       prob=0.95)+
  labs(title="Overall Health Rating") +
  scale_x_continuous(breaks=c(1,2,3,4),
                     labels=c("Excellent","Good", "Fair","Poor")) 
ppcplot3<-ppcplot(ppc3)
ggsave(file="~/ppcplot3.png",
       ppcplot3,width=200,height=130,units="mm")

# PPC4 - Left Hand Grip 
lhand_na<-which(is.na(BaselineData$hand_grip_left_0))
yrep4<-yrep4[,-lhand_na]
y4<-BaselineData %>%
  select(c("hand_grip_left_0","Diagnosis")) %>% 
  filter(!is.na(hand_grip_left_0)) 
ppc4<-ppc_dens_overlay_grouped(y4$hand_grip_left_0,
                               yrep4,
                               group=y4$Diagnosis)+
  labs(title="Left Hand Grip Strength (kg)")
ppcplot4<-ppcplot(ppc4)
ggsave(file="~/ppcplot4.png",
       ppcplot4,width=200,height=130,units="mm")

# PPC5 - Numeric Memory 
yrep5<-round(yrep5)
y5<-BaselineData %>%
  select(c("numeric_memory_max_digits_remembered_0","Diagnosis")) %>% 
  filter(!is.na(numeric_memory_max_digits_remembered_0)) 
ppc5<-ppc_bars_grouped(y5$numeric_memory_max_digits_remembered_0,
                       yrep5,
                       group=y5$Diagnosis,
                       freq=FALSE,
                       prob=0.95)+
  labs(title="Numeric Memory") +
  scale_x_continuous(breaks=seq(0,16,2))
ppcplot5<-ppcplot(ppc5)
ggsave(file="~/ppcplot5.png",
       ppcplot5,width=200,height=130,units="mm")

# PPC6 - Pairs Matching 1
yrep6<-round(yrep6)
pairs1_na<-which(is.na(BaselineData$pairs_matching_0_1))
yrep6<-yrep6[,-pairs1_na]
y6<-BaselineData %>%
  select(c("pairs_matching_0_1","Diagnosis")) %>% 
  filter(!is.na(pairs_matching_0_1)) 
ppc6<-ppc_bars_grouped(y6$pairs_matching_0_1,
                       yrep6,
                       group=y6$Diagnosis,
                       freq=FALSE,
                       prob=0.95)+
  labs(title="Pairs Matching - Round 1",
       x="No. of Incorrect Matches")+
  coord_cartesian(xlim = c(-1, 10))
ppcplot6<-ppcplot(ppc6)
ggsave(file="~/ppcplot6.png",
       ppcplot6,width=200,height=130,units="mm")

# PPC7 - Pairs Matching 2
yrep7<-round(yrep7)
pairs2_na<-which(is.na(BaselineData$pairs_matching_0_2))
yrep7<-yrep7[,-pairs2_na]
y7<-BaselineData %>%
  select(c("pairs_matching_0_2","Diagnosis")) %>% 
  filter(!is.na(pairs_matching_0_2)) 
ppc7<-ppc_bars_grouped(y7$pairs_matching_0_2,
                       yrep7,
                       group=y7$Diagnosis,
                       freq=FALSE,
                       prob=0.95)+
  labs(title="Pairs Matching - Round 2",
       x="No. of Incorrect Matches")+
  coord_cartesian(xlim = c(-1, 15))
ppcplot7<-ppcplot(ppc7)
ggsave(file="~/ppcplot7.png",
       ppcplot7,width=200,height=130,units="mm")

# PPC8 - Prospective Memory
y8<-BaselineData %>%
  select(c("prospective_memory_0","Diagnosis")) %>% 
  filter(!is.na(prospective_memory_0)) %>%
  mutate(prospective_memory_0 = as.numeric(prospective_memory_0))
ppc8<-ppc_bars_grouped(y8$prospective_memory_0,
                       yrep8,
                       group=y8$Diagnosis,
                       freq=FALSE,
                       prob=0.95)+
  labs(title="Prospective Memory")+
  scale_x_continuous(breaks=c(1,2,3),
                     labels=c("Recall\non 1st\nattempt",
                              "Recall\non 2nd\nattempt",
                              "Not\nrecalled"))
ppcplot8<-ppcplot(ppc8)
ggsave(file="~/ppcplot8.png",
       ppcplot8,width=200,height=130,units="mm")

# PPC9 - Right Hand Grip 
rhand_na<-which(is.na(BaselineData$hand_grip_right_0))
yrep9<-yrep9[,-rhand_na]
y9<-BaselineData %>%
  select(c("hand_grip_right_0","Diagnosis")) %>% 
  filter(!is.na(hand_grip_right_0)) 
ppc9<-ppc_dens_overlay_grouped(y9$hand_grip_right_0,
                               yrep9,
                               group=y9$Diagnosis)+
  labs(title="Right Hand Grip Strength (kg)")
ppcplot9<-ppcplot(ppc9)
ggsave(file="~/ppcplot9.png",
       ppcplot9,width=200,height=130,units="mm")

# PPC10 - Reaction Time
rxn_na<-which(is.na(BaselineData$reaction_time_mean_time_to_correctly_identify_matches_0))
yrep10<-yrep10[,-rxn_na]
y10<-BaselineData %>%
  select(c("reaction_time_mean_time_to_correctly_identify_matches_0","Diagnosis")) %>% 
  filter(!is.na(reaction_time_mean_time_to_correctly_identify_matches_0)) 
ppc10<-ppc_dens_overlay_grouped(y10$reaction_time_mean_time_to_correctly_identify_matches_0,
                               yrep10,
                               group=y10$Diagnosis)+
  labs(title="Reaction Time",
       subtitle="Mean Time to Correctly Identify Matches")+
  coord_cartesian(xlim=c(0,2000))
ppcplot10<-ppcplot(ppc10)
ggsave(file="~/ppcplot10.png",
       ppcplot10,width=200,height=130,units="mm")

# PPC11 - Weight Change 
weight_na<-which(is.na(BaselineData$weight_change_compared_with_1_year_ago_0))
yrep11<-yrep11[,-weight_na]
yrep11[yrep11 == 0]<-1
y11<-BaselineData %>%
  select(c("weight_change_compared_with_1_year_ago_0","Diagnosis")) %>%
  filter(!is.na(weight_change_compared_with_1_year_ago_0)) %>% 
  mutate(weight_change_compared_with_1_year_ago_0 = as.numeric(weight_change_compared_with_1_year_ago_0))%>%
  mutate(weight_change_compared_with_1_year_ago_0 = case_when(
    weight_change_compared_with_1_year_ago_0 == 0 ~ 1,
    TRUE ~ weight_change_compared_with_1_year_ago_0 
  ))
ppc11<-ppc_bars_grouped(y11$weight_change_compared_with_1_year_ago_0,
                       yrep11,
                       group=y11$Diagnosis,
                       freq=FALSE,
                       prob=0.95)+
  labs(title="Weight Change Compared with 1 Year Ago") +
  scale_x_continuous(breaks=c(1,2,3),
                     labels=c("Same\nWeight",
                              "Gained\nWeight",
                              "Lost\nWeight")) +
  coord_cartesian(xlim=c(0.5,3.5))
ppcplot11<-ppcplot(ppc11)
ggsave(file="~/ppcplot11.png",
       ppcplot11,width=200,height=130,units="mm")
