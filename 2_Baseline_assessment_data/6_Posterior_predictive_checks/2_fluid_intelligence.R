library(brms)
library(dplyr)
fluid1<-readRDS("~/fluid1.rds")
y_rep<-posterior_predict(fluid1,subset=c(500,10500,20500,30500,40500,
                                         9000,19000,29000,39000,49000))
saveRDS(y_rep,file="~yrep2")