---
title: "R Notebook"
output:
  html_document:
    df_print: paged
---

```{r}
library(dplyr)

# Read a chunk of biobank data 
chunk5<-read.csv("~/Project/ukb.csv",nrows=100000,skip=400001,check.names = FALSE,header=FALSE)

# Get headers 
headerchunk<-read.csv("~/Project/ukb.csv",nrows=2,skip=0,check.names = FALSE)
headers<-names(headerchunk)
colnames(chunk5) = headers 

# Select data fields for timeline analysis 
timelinevector<-readRDS("~/Project/timelinevector.rds")
chunk5timeline<- chunk5 %>% select(all_of(timelinevector))
saveRDS(chunk5timeline, file = "~/Scratch/timeline5.rds")
```

