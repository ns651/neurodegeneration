---
title: "R Notebook"
output:
  html_document:
    df_print: paged
  html_notebook: default
  pdf_document: default
---

```{r}
library(ggplot2)
library(lubridate)
library(MASS)
library(lmerTest)
library(nnet)
library(dplyr)

root_dir = "~/Project/"
timeline <- readRDS(paste(root_dir,
                          "timeline.rds",
                          sep = "/"))

figwidth = 150
figheight = 75 
```

```{r}
# Remove those with self-reported diagnosis at baseline assessment
srbaseline<-timeline %>%
  filter(year(date_of_assessment_0) == sr_year)

timeline <- timeline %>%
  filter(!eid %in% srbaseline$eid)
```


```{r}
# Consolidate data from the 3 assessment visits into a common format

endings <- c("0","1","2","3")
timelinedata <- lapply(endings,
                       function(i) timeline %>% 
                         select(c("eid", "Diagnosis",
                                  ends_with(!!i))) %>%
                         rename_with(~gsub(i, "", .),
                                     ends_with(!!i))) 

graphdata <- bind_rows(timelinedata) %>%
  rename_with(~gsub("_","",.),
              ends_with("_")) %>%
  filter(diagyears <= 0) %>% # filter by only assessment prior to diagnosis
  mutate(eid = as.factor(eid))
```

```{r}
# Multinomial Regression for Weight Change in Past Year 
wt_data <- graphdata %>% 
  select(c("eid",
           "Diagnosis",
           "age",
           "diagyears",
           "wt")) %>%
  filter(!is.na(wt))

wt_lm <- multinom(formula = wt ~ age,
               data = wt_data)

wt_data <- wt_data %>% 
  filter(Diagnosis != "Control")

# Define offset matrix - to fix coefficient for age 
wt_coef<-c(0,coef(wt_lm)[[1,2]],coef(wt_lm)[[2,2]])
wt_mat<-matrix(wt_coef,nrow=1,ncol=3,byrow=TRUE)

# Plot weight change against years of diagnosis 
for (diag in levels(wt_data$Diagnosis)[-1]) {
  data_diag <- subset(wt_data,
                      Diagnosis == diag)
  wt_resid_model <- multinom(wt ~ diagyears +
                                  offset(age%*%wt_mat),
                              data = subset(wt_data,
                                            Diagnosis == diag),
                              model=TRUE)
  wt_coef<-summary(wt_resid_model)$coefficients/summary(wt_resid_model)$standard.errors
  wt_p <- (1 - pnorm(abs(wt_coef), 0, 1)) * 2
  colnames(wt_p) <- c("intercept_p_value","diagyears_p_value")
  
  print(diag)
  print(summary(wt_resid_model))
  print(wt_p)

  wt_plot <- wt_data %>%
    filter(Diagnosis == diag) %>%
    ggplot(aes(y = wt,
               x = diagyears)) +
    geom_point(alpha = 0.2,
               position = position_dodge2(width = 0.1)) +
    labs(y = "Weight Change In Past Year",
         x = "Years of Diagnosis",
         title = paste("Weight Change in Past Year", diag)) +
    geom_line(aes(group = eid)) +
    #geom_abline(intercept = mod_intercept,
    #            slope = mod_slope,
    #            colour = "blue") +
    #facet_wrap(vars(Diagnosis)) +
    theme(plot.title = element_text(hjust = 0.5))
  print(wt_plot)

  ggsave(file = paste(root_dir,
                      paste("Timeline/wt_resid_",
                            diag,
                            ".png",
                            sep = ""),
                      sep = "/"),
         wt_plot,
         width = figwidth,
         height = figheight,
         units = "mm")
  
  saveRDS(wt_resid_model,
          file = paste(root_dir,
                      paste("Timeline/wt_",
                            diag,
                            ".rds",
                            sep = ""),
                      sep = "/"))
}

rm(list = c("wt_data",
            "wt_lm",
            "wt_plot",
            "wt_coef",
            "wt_mat"))
```


```{r}
# Ordinal Regression for Falls in Past Year 
falls_data <- graphdata %>% 
  select(c("eid",
           "Diagnosis",
           "age",
           "diagyears",
           "falls")) %>%
  filter(!is.na(falls))

falls_lm <- polr(formula = falls ~ age,
               data = falls_data)

falls_data <- falls_data %>% 
  filter(Diagnosis != "Control")

# Plot falls residuals against years of diagnosis 
for (diag in levels(falls_data$Diagnosis)[-1]) {
  data_diag <- subset(falls_data,
                      Diagnosis == diag)
  falls_resid_model <- polr(falls ~ diagyears +
                                      offset(age*coef(falls_lm)[[1]]),
                              data = subset(falls_data,
                                            Diagnosis == diag),
                              Hess = TRUE)
  print(diag)
  print(summary(falls_resid_model))
  falls_coef <- coef(summary(falls_resid_model)) # extract coefficients with std error and t values
  falls_p <- pnorm(abs(falls_coef[, "t value"]), lower.tail = FALSE) * 2 # calculate p values
  falls_p_table <- cbind(falls_coef, "p_value" = falls_p) # add p values to table
  print("P Values")
  print(falls_p_table)

  falls_plot <- falls_data %>%
    filter(Diagnosis == diag) %>%
    ggplot(aes(y = falls,
               x = diagyears)) +
    geom_line(aes(group = eid),
              colour = "gray40") +
    geom_point(alpha = 0.2,
               position = position_dodge2(width = 0.1)) +
    labs(y = "No. of Falls In Past Year",
         x = "Years of Diagnosis",
         title = paste("Falls In Past Year", diag)) +
    #geom_abline(intercept = mod_intercept,
    #            slope = mod_slope,
    #            colour = "blue") +
    #facet_wrap(vars(Diagnosis)) +
    theme(plot.title = element_text(hjust = 0.5))
  print(falls_plot)

  ggsave(file = paste(root_dir,
                      paste("Timeline/falls_resid_",
                            diag,
                            ".png",
                            sep = ""),
                      sep = "/"),
         falls_plot,
         width = figwidth,
         height = figheight,
         units = "mm")
  
    saveRDS(falls_resid_model,
          file = paste(root_dir,
                      paste("Timeline/falls_",
                            diag,
                            ".rds",
                            sep = ""),
                      sep = "/"))
}

rm(list = c("falls_data",
            "falls_lm",
            "falls_plot",
            "falls_coef",
            "falls_p"))
```

```{r}
# Ordinal Regression for Overall Health Rating
health_data <- graphdata %>% 
  select(c("eid",
           "Diagnosis",
           "age",
           "diagyears",
           "health")) %>%
  filter(!is.na(health))

health_lm <- polr(formula = health ~ age,
               data = health_data)

health_data <- health_data %>% 
  filter(Diagnosis != "Control")

# Plot health residuals against years of diagnosis 
for (diag in levels(health_data$Diagnosis)[-1]) {
  data_diag <- subset(health_data,
                      Diagnosis == diag)
  health_resid_model <- polr(health ~ diagyears +
                                      offset(age*coef(health_lm)[[1]]),
                              data = subset(health_data,
                                            Diagnosis == diag),
                              Hess = TRUE)
  print(diag)
  print(summary(health_resid_model))
 health_coef <- coef(summary(health_resid_model)) # extract coefficients with std error and t values
  health_p <- pnorm(abs(health_coef[, "t value"]), lower.tail = FALSE) * 2 # calculate p values
  health_p_table <- cbind(health_coef, "p_value" = health_p) # add p values to table
  print("P Values")
  print(health_p_table)

  health_plot <- health_data %>%
    filter(Diagnosis == diag) %>%
    ggplot(aes(y = health,
               x = diagyears)) +
    geom_line(aes(group = eid),
              colour = "gray40") +
    geom_point(alpha = 0.2,
               position = position_dodge2(width = 0.1)) +
    labs(y = "Overall Health Rating",
         x = "Years of Diagnosis",
         title = paste("Overall Health Rating", diag)) +
    #geom_abline(intercept = mod_intercept,
    #            slope = mod_slope,
    #            colour = "blue") +
    #facet_wrap(vars(Diagnosis)) +
    theme(plot.title = element_text(hjust = 0.5))
  print(health_plot)

  ggsave(file = paste(root_dir,
                      paste("Timeline/health_resid_",
                            diag,
                            ".png",
                            sep = ""),
                      sep = "/"),
         health_plot,
         width = figwidth,
         height = figheight,
         units = "mm")
  
  saveRDS(health_resid_model,
          file = paste(root_dir,
                      paste("Timeline/health_",
                            diag,
                            ".rds",
                            sep = ""),
                      sep = "/"))  
}

rm(list = c("health_data",
            "health_lm",
            "health_plot",
            "health_coef",
            "health_p"))
```

```{r}
# Ordinal Regression for Prospective Memory
pros_data <- graphdata %>% 
  select(c("eid",
           "Diagnosis",
           "age",
           "diagyears",
           "pros")) %>%
  filter(!is.na(pros))

pros_lm <- polr(formula = pros ~ age,
               data = pros_data)

pros_data <- pros_data %>% 
  filter(Diagnosis != "Control")

# Plot Prospective Memory residuals against years of diagnosis 
for (diag in levels(pros_data$Diagnosis)[-1]) {
  data_diag <- subset(pros_data,
                      Diagnosis == diag)
  pros_resid_model <- polr(pros ~ diagyears +
                                      offset(age*coef(pros_lm)[[1]]),
                              data = subset(pros_data,
                                            Diagnosis == diag),
                              Hess = TRUE)
  print(diag)
  print(summary(pros_resid_model))
  pros_coef <- coef(summary(pros_resid_model)) # extract coefficients with std error and t values
  pros_p <- pnorm(abs(pros_coef[, "t value"]), lower.tail = FALSE) * 2 # calculate p values
  pros_p_table <- cbind(pros_coef, "p_value" = pros_p) # add p values to table
  print("P Values")
  print(pros_p_table)
  
  pros_plot <- pros_data %>%
    filter(Diagnosis == diag) %>%
    ggplot(aes(y = pros,
               x = diagyears)) +
    geom_line(aes(group = eid),
              colour = "gray40") +
    geom_point(alpha = 0.2,
               position = position_dodge2(width = 0.1)) +
    labs(y = "Prospective Memory",
         x = "Years of Diagnosis",
         title = paste("Prospective Memory", diag)) +
    #geom_abline(intercept = mod_intercept,
    #            slope = mod_slope,
    #            colour = "blue") +
    #facet_wrap(vars(Diagnosis)) +
    theme(plot.title = element_text(hjust = 0.5))
  print(pros_plot)

  ggsave(file = paste(root_dir,
                      paste("Timeline/pros_resid_",
                            diag,
                            ".png",
                            sep = ""),
                      sep = "/"),
         pros_plot,
         width = figwidth,
         height = figheight,
         units = "mm")
    saveRDS(pros_resid_model,
          file = paste(root_dir,
                      paste("Timeline/pros_",
                            diag,
                            ".rds",
                            sep = ""),
                      sep = "/"))  
}

rm(list = c("pros_data",
            "pros_lm",
            "pros_plot",
            "pros_coef",
            "pros_p"))
```


```{r}
# Linear model for fluid intelligence 
fluid_data <- graphdata %>% 
  select(c("eid",
           "Diagnosis",
           "age",
           "diagyears",
           "fluid")) %>%
  filter(!is.na(fluid))

fluid_lm <- lmer(formula = fluid ~ age + (1|eid),
               data = fluid_data)

fluid_data <- fluid_data %>% 
   mutate(resid = resid(fluid_lm)) %>% 
  filter(Diagnosis != "Control")

# Plot fluid intelligence score residuals against years of diagnosis 
for (diag in levels(fluid_data$Diagnosis)[-1]) {
  data_diag <- subset(fluid_data,
                      Diagnosis == diag)
  if (length(unique(data_diag$eid)) < nrow(data_diag) - 5) {
    fluid_resid_model <- lmer(resid ~ diagyears +(1|eid),
                              data = subset(fluid_data,
                                            Diagnosis == diag))
    mod_intercept = fluid_resid_model@pp$delb[[1]]
    mod_slope = fluid_resid_model@pp$delb[[2]]
    
    print(diag)
    print(summary(fluid_resid_model))
    anova_result <- anova(fluid_resid_model)
    print(anova_result)
  } else {
    fluid_resid_model <- lm(resid ~ diagyears,
                            data = subset(fluid_data,
                                          Diagnosis == diag))
    mod_intercept = fluid_resid_model$coefficients[["(Intercept)"]]
    mod_slope = fluid_resid_model$coefficients[["diagyears"]]
    print(diag)
    print(summary(fluid_resid_model))
  } 
  
  fluid_plot <- fluid_data %>%
    filter(Diagnosis == diag) %>%
    ggplot(aes(y = resid,
               x = diagyears)) +
    geom_point(alpha = 0.2) +
    labs(y = "Residual",
         x = "Years of Diagnosis",
         title = paste("Fluid Intelligence Score", diag)) +
    geom_line(aes(group = eid),
              colour = "gray40") +
    geom_abline(intercept = mod_intercept,
                slope = mod_slope,
                colour = "blue") +
    #facet_wrap(vars(Diagnosis)) +
    theme(plot.title = element_text(hjust = 0.5))
  print(fluid_plot)

  ggsave(file = paste(root_dir,
                      paste("Timeline/fluid_resid_",
                            diag,
                            ".png",
                            sep = ""),
                      sep = "/"),
         fluid_plot,
         width = figwidth,
         height = figheight,
         units = "mm")
  
  saveRDS(fluid_resid_model,
          file = paste(root_dir,
                      paste("Timeline/fluid_",
                            diag,
                            ".rds",
                            sep = ""),
                      sep = "/"))
}

rm(list = c("fluid_data",
            "fluid_lm",
            "fluid_plot"))
```

```{r}
# Linear model for numeric memory
num_data <- graphdata %>% 
  select(c("eid", "Diagnosis","age","diagyears","num")) %>%
  filter(!is.na(num))

num_lm <- lmer(formula = num ~ age + (1|eid),
           data = num_data)

num_data <- num_data %>% 
  mutate(resid = resid(num_lm)) %>% 
  filter(Diagnosis != "Control")

for (diag in levels(num_data$Diagnosis)[-1]) {
  data_diag <- subset(num_data,
                      Diagnosis == diag)
  if (length(unique(data_diag$eid)) < nrow(data_diag) - 5) {
    num_resid_model <- lmer(resid ~ diagyears +(1|eid),
                              data = subset(num_data,
                                            Diagnosis == diag))
    mod_intercept = num_resid_model@pp$delb[[1]]
    mod_slope = num_resid_model@pp$delb[[2]]
    
    print(diag)
    print(summary(num_resid_model))
    anova_result <- anova(num_resid_model)
    print(anova_result)
  } else {
    num_resid_model <- lm(resid ~ diagyears,
                            data = subset(num_data,
                                          Diagnosis == diag))
    mod_intercept = num_resid_model$coefficients[["(Intercept)"]]
    mod_slope = num_resid_model$coefficients[["diagyears"]]
    print(diag)
    print(summary(num_resid_model))
  } 
  
  # Plot numeric memory residuals against years of diagnosis 
  num_plot <- num_data %>%
    filter(Diagnosis == diag) %>%
    ggplot(aes(y = resid,
               x = diagyears)) +
    geom_point(alpha = 0.2) +
    labs(y = "Residual",
         x = "Years of Diagnosis",
         title = paste("Numeric Memory", diag)) +
    # facet_wrap(vars(Diagnosis)) +
    geom_line(aes(group = eid),
              colour = "gray40") +
    geom_abline(intercept = mod_intercept,
                slope = mod_slope,
                colour = "blue") +
    theme(plot.title = element_text(hjust = 0.5))
  print(num_plot)

  ggsave(file = paste(root_dir,
                      paste("Timeline/num_resid_",
                            diag,
                            ".png",
                            sep = ""),
                      sep = "/"),
         num_plot,
         width = figwidth,
         height = figheight,
         units = "mm")
  
  saveRDS(num_resid_model,
          file = paste(root_dir,
                      paste("Timeline/num_",
                            diag,
                            ".rds",
                            sep = ""),
                      sep = "/"))
}

rm(list = c("num_data",
            "num_lm",
            "num_plot"))
```

# Linear model for reaction time 
```{r}
# Linear model for reaction time 
rxn_data <- graphdata %>% 
  select(c("eid", 
           "Diagnosis",
           "age",
           "diagyears",
           "rxn")) %>%
  filter(!is.na(rxn))

rxn_lm <- lmer(formula = rxn ~ age + (1|eid),
               data = rxn_data)

rxn_data <- rxn_data %>% 
  mutate(resid = resid(rxn_lm)) %>% 
  filter(Diagnosis != "Control")

for (diag in levels(rxn_data$Diagnosis)[-1]) {
  data_diag <- subset(rxn_data,
                      Diagnosis == diag)
  if (length(unique(data_diag$eid)) < nrow(data_diag) - 5) {
    rxn_resid_model <- lmer(resid ~ diagyears +(1|eid),
                              data = subset(rxn_data,
                                            Diagnosis == diag))
    mod_intercept = rxn_resid_model@pp$delb[[1]]
    mod_slope = rxn_resid_model@pp$delb[[2]]
    
    print(diag)
    print(summary(rxn_resid_model))
    anova_result <- anova(rxn_resid_model)
    print(anova_result)
  } else {
    rxn_resid_model <- lm(resid ~ diagyears,
                            data = subset(rxn_data,
                                          Diagnosis == diag))
    mod_intercept = rxn_resid_model$coefficients[["(Intercept)"]]
    mod_slope = rxn_resid_model$coefficients[["diagyears"]]
    print(diag)
    print(summary(rxn_resid_model))
  } 
  
  # Plot reaction time residuals against years of diagnosis 
  rxn_plot <- rxn_data %>%
    filter(Diagnosis == diag) %>%
    ggplot(aes(y = resid,
               x = diagyears)) +
    geom_point(alpha = 0.2) +
    labs(y = "Residual",
         x = "Years of Diagnosis",
         title = paste("Reaction time", diag)) +
    # facet_wrap(vars(Diagnosis)) +
    geom_line(aes(group = eid),
              colour = "gray40") +
    geom_abline(intercept = mod_intercept,
                slope = mod_slope,
                colour = "blue") +
    theme(plot.title = element_text(hjust = 0.5))
  print(rxn_plot)

  ggsave(file = paste(root_dir,
                      paste("Timeline/rxn_resid_",
                            diag,
                            ".png",
                            sep = ""),
                      sep = "/"),
         rxn_plot,
         width = figwidth,
         height = figheight,
         units = "mm")
  
  saveRDS(rxn_resid_model,
          file = paste(root_dir,
                      paste("Timeline/rxn_",
                            diag,
                            ".rds",
                            sep = ""),
                      sep = "/"))
}

rm(list = c("rxn_data",
            "rxn_lm",
            "rxn_plot"))
```

# Linear model for pairs matching 1 
```{r}
# Linear model for pairs matching 1 
pairs1_data <- graphdata %>% 
  select(c("eid", 
           "Diagnosis",
           "age",
           "diagyears",
           "pairs1")) %>%
  filter(!is.na(pairs1))

pairs1_lm <- lmer(formula = pairs1 ~ age + (1|eid),
                  data = pairs1_data)

pairs1_data <- pairs1_data %>% 
  mutate(resid = resid(pairs1_lm)) %>% 
  filter(Diagnosis != "Control")

for (diag in levels(pairs1_data$Diagnosis)[-1]) {
  data_diag <- subset(pairs1_data,
                      Diagnosis == diag)
  if (length(unique(data_diag$eid)) < nrow(data_diag) - 5) {
    pairs1_resid_model <- lmer(resid ~ diagyears +(1|eid),
                              data = subset(pairs1_data,
                                            Diagnosis == diag))
    mod_intercept = pairs1_resid_model@pp$delb[[1]]
    mod_slope = pairs1_resid_model@pp$delb[[2]]
    
    print(diag)
    print(summary(pairs1_resid_model))
    anova_result <- anova(pairs1_resid_model)
    print(anova_result)
  } else {
    pairs1_resid_model <- lm(resid ~ diagyears,
                            data = subset(pairs1_data,
                                          Diagnosis == diag))
    mod_intercept = pairs1_resid_model$coefficients[["(Intercept)"]]
    mod_slope = pairs1_resid_model$coefficients[["diagyears"]]
    print(diag)
    print(summary(pairs1_resid_model))
  } 
  
  # Plot pairs matching 1 residuals against years of diagnosis 
  pairs1_plot <- pairs1_data %>%
    filter(Diagnosis == diag) %>%
    ggplot(aes(y = resid,
               x = diagyears)) +
    geom_point(alpha = 0.2) +
    labs(y = "Residual",
         x = "Years of Diagnosis",
         title = paste("Pairs matching 1", diag)) +
    # facet_wrap(vars(Diagnosis)) +
    geom_line(aes(group = eid),
              colour = "gray40") +
    geom_abline(intercept = mod_intercept,
                slope = mod_slope,
                colour = "blue") +
    theme(plot.title = element_text(hjust = 0.5))
  
  print(pairs1_plot)

  ggsave(file = paste(root_dir,
                      paste("Timeline/pairs1_resid_",
                            diag,
                            ".png",
                            sep = ""),
                      sep = "/"),
         pairs1_plot,
         width = figwidth,
         height = figheight,
         units = "mm")
  
  saveRDS(pairs1_resid_model,
          file = paste(root_dir,
                      paste("Timeline/pairs1_",
                            diag,
                            ".rds",
                            sep = ""),
                      sep = "/"))
}

rm(list = c("pairs1_data",
            "pairs1_lm",
            "pairs1_plot"))
```


# Linear model for pairs matching 2 

```{r}
# Linear model for pairs matching 2 
pairs2_data <- graphdata %>% 
  select(c("eid", 
           "Diagnosis",
           "age",
           "diagyears",
           "pairs2")) %>%
  filter(!is.na(pairs2))

pairs2_lm <- lmer(formula = pairs2 ~ age + (1|eid),
                data = pairs2_data)

pairs2_data <- pairs2_data %>% 
  mutate(resid = resid(pairs2_lm)) %>% 
  filter(Diagnosis != "Control")

for (diag in levels(pairs2_data$Diagnosis)[-1]) {
  data_diag <- subset(pairs2_data,
                      Diagnosis == diag)
  if (length(unique(data_diag$eid)) < nrow(data_diag) - 5) {
    pairs2_resid_model <- lmer(resid ~ diagyears +(1|eid),
                              data = subset(pairs2_data,
                                            Diagnosis == diag))
    mod_intercept = pairs2_resid_model@pp$delb[[1]]
    mod_slope = pairs2_resid_model@pp$delb[[2]]
    
    print(diag)
    print(summary(pairs2_resid_model))
    anova_result <- anova(pairs2_resid_model)
    print(anova_result)
  } else {
    pairs2_resid_model <- lm(resid ~ diagyears,
                            data = subset(pairs2_data,
                                          Diagnosis == diag))
    mod_intercept = pairs2_resid_model$coefficients[["(Intercept)"]]
    mod_slope = pairs2_resid_model$coefficients[["diagyears"]]
    print(diag)
    print(summary(pairs2_resid_model))
  } 
  
  # Plot pairs matching 2 against years of diagnosis 
  pairs2_plot <- pairs2_data %>%
    filter(Diagnosis == diag) %>%
    ggplot(aes(y = resid,
               x = diagyears)) +
    geom_point(alpha = 0.2) +
    labs(y = "Residual",
         x = "Years of Diagnosis",
         title = paste("Pairs matching 2", diag)) +
    # facet_wrap(vars(Diagnosis)) +
    geom_line(aes(group = eid),
              colour = "gray40") +
    geom_abline(intercept = mod_intercept,
                slope = mod_slope,
                colour = "blue") +
    theme(plot.title = element_text(hjust = 0.5))
  
  print(pairs2_plot)

  ggsave(file = paste(root_dir,
                      paste("Timeline/pairs2_resid_",
                            diag,
                            ".png",
                            sep = ""),
                      sep = "/"),
         pairs2_plot,
         width = figwidth,
         height = figheight,
         units = "mm")
  
  saveRDS(pairs2_resid_model,
          file = paste(root_dir,
                      paste("Timeline/pairs2_",
                            diag,
                            ".rds",
                            sep = ""),
                      sep = "/"))
}

rm(list = c("pairs2_data",
            "pairs2_lm",
            "pairs2_plot"))
```


Linear model for left hand grip strength  
```{r}
# Linear model for left hand grip strength  
lhand_data <- graphdata %>% 
  select(c("eid", 
           "Diagnosis",
           "age",
           "diagyears",
           "lhand")) %>%
  filter(!is.na(lhand))

lhand_lm <- lmer(formula = lhand ~ age + (1|eid),
                 data = lhand_data)

lhand_data <- lhand_data %>% 
  mutate(resid = resid(lhand_lm)) %>% 
  filter(Diagnosis != "Control")

for (diag in levels(lhand_data$Diagnosis)[-1]) {
  data_diag <- subset(lhand_data,
                      Diagnosis == diag)
  if (length(unique(data_diag$eid)) < nrow(data_diag) - 5) {
    lhand_resid_model <- lmer(resid ~ diagyears +(1|eid),
                              data = subset(lhand_data,
                                            Diagnosis == diag))
    mod_intercept = lhand_resid_model@pp$delb[[1]]
    mod_slope = lhand_resid_model@pp$delb[[2]]
    
    print(diag)
    print(summary(lhand_resid_model))
    anova_result <- anova(lhand_resid_model)
    print(anova_result)
  } else {
    lhand_resid_model <- lm(resid ~ diagyears,
                            data = subset(lhand_data,
                                          Diagnosis == diag))
    mod_intercept = lhand_resid_model$coefficients[["(Intercept)"]]
    mod_slope = lhand_resid_model$coefficients[["diagyears"]]
    print(diag)
    print(summary(lhand_resid_model))
    } 
  
  # Plot pairs matching 2 against years of diagnosis 
  lhand_plot <- lhand_data %>%
    filter(Diagnosis == diag) %>%
    ggplot(aes(y = resid,
               x = diagyears)) +
    geom_point(alpha = 0.2) +
    labs(y = "Residual",
         x = "Years of Diagnosis",
         title = paste("Left hand grip strength", diag)) +
    # facet_wrap(vars(Diagnosis)) +
    geom_line(aes(group = eid),
              colour = "gray40") +
    geom_abline(intercept = mod_intercept,
                slope = mod_slope,
                colour = "blue") +
    theme(plot.title = element_text(hjust = 0.5))
  
  print(lhand_plot)

  ggsave(file = paste(root_dir,
                      paste("Timeline/lhand_resid_",
                            diag,
                            ".png",
                            sep = ""),
                      sep = "/"),
         lhand_plot,
         width = figwidth,
         height = figheight,
         units = "mm")
  
  saveRDS(lhand_resid_model,
          file = paste(root_dir,
                      paste("Timeline/lhand_",
                            diag,
                            ".rds",
                            sep = ""),
                      sep = "/"))
}

rm(list = c("lhand_data",
            "lhand_lm",
            "lhand_plot"))
```

# Linear model for right hand grip strength
```{r}
# Linear model for right hand grip strength
rhand_data <- graphdata %>% 
  select(c("eid", 
           "Diagnosis",
           "age",
           "diagyears",
           "rhand")) %>%
  filter(!is.na(rhand))

rhand_lm <- lmer(formula = rhand ~ age + (1|eid),
                 data = rhand_data)

rhand_data <- rhand_data %>% 
  mutate(resid = resid(rhand_lm)) %>% 
  filter(Diagnosis != "Control")

for (diag in levels(rhand_data$Diagnosis)[-1]) {
  data_diag <- subset(rhand_data,
                      Diagnosis == diag)
  if (length(unique(data_diag$eid)) < nrow(data_diag) - 5) {
    rhand_resid_model <- lmer(resid ~ diagyears +(1|eid),
                              data = subset(rhand_data,
                                            Diagnosis == diag))
    mod_intercept = rhand_resid_model@pp$delb[[1]]
    mod_slope = rhand_resid_model@pp$delb[[2]]
    
    print(diag)
    print(summary(rhand_resid_model))
    anova_result <- anova(rhand_resid_model)
    print(anova_result)
  } else {
    rhand_resid_model <- lm(resid ~ diagyears,
                            data = subset(rhand_data,
                                          Diagnosis == diag))
    mod_intercept = rhand_resid_model$coefficients[["(Intercept)"]]
    mod_slope = rhand_resid_model$coefficients[["diagyears"]]
    print(diag)
    print(summary(rhand_resid_model))
      } 
  
  # Plot pairs matching 2 against years of diagnosis 
  rhand_plot <- rhand_data %>%
    filter(Diagnosis == diag) %>%
    ggplot(aes(y = resid,
               x = diagyears)) +
    geom_point(alpha = 0.2) +
    labs(y = "Residual",
         x = "Years of Diagnosis",
         title = paste("Right hand grip strength", diag)) +
    # facet_wrap(vars(Diagnosis)) +
    geom_line(aes(group = eid),
              colour = "gray40") +
    geom_abline(intercept = mod_intercept,
                slope = mod_slope,
                colour = "blue") +
    theme(plot.title = element_text(hjust = 0.5))
  
  print(rhand_plot)

  ggsave(file = paste(root_dir,
                      paste("Timeline/rhand_resid_",
                            diag,
                            ".png",
                            sep = ""),
                      sep = "/"),
         rhand_plot,
         width = figwidth,
         height = figheight,
         units = "mm")

  saveRDS(rhand_resid_model,
          file = paste(root_dir,
                      paste("Timeline/rhand_",
                            diag,
                            ".rds",
                            sep = ""),
                      sep = "/"))
}

rm(list = c("rhand_data",
            "rhand_lm",
            "rhand_plot"))
```
